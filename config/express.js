/*
 * 24th Jun, 2018
 * Express.js - initlialize the Routes and other 
 * requirements of the system. 
 * @author: Ankit Singh <ank.it@live.com>
 */


const express = require('express')
const bodyParser = require('body-parser')
const compress = require('compression')
const cors = require('cors')
const Routes = require('../public-api')
const config = require('./env')
const app = express()
const Controller = require('../server/controllers')

app.use('/favicon.ico', (req, res, next) => {
  res.send();
});

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(compress());

// enable CORS
app.use(cors());
app.use('/api/v1', Routes.APIRoutes);
app.use('/api/admin', Routes.AdminRoutes)

app.use((req, res, next) => {
  const err = new Error('Not Found');
  err.status = 404;
  next(err);
});

app.use((err, req, res, next) => {
  res.status(err.status || 500);
  res.json({
    error: {
      message: err.message
    }
  });
});


module.exports = app;