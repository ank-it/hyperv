/* 
 * 24th Jun, 2018
 * @description: Expose the required modules inside
 * as per the requirement. 
 * 
 */

exports.APIRoutes =	require("./common");
exports.AdminRoutes = require("./admin");