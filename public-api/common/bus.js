/*
	@author: Ankit Singh<ankit@wizely.in>
 */

const Controller = require('../../server/controllers')
const router 	 = require('express').Router()

router.get('/hello', (req, res, next) => {
	res.send('World');
});

router.route('/bus')
	.post(Controller.Bus.create)
	.get(Controller.Bus.get);

module.exports = router;
