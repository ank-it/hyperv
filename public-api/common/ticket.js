/*
	@author: Ankit Singh<ankit@wizely.in>
 */

const Controller = require('../../server/controllers')
const router 	 = require('express').Router()


router.route('/ticket')
	.post(Controller.Ticket.create)
	.get(Controller.Ticket.get);

router.route('/ticket/:id')
	.get(Controller.Ticket.find)
	.put(Controller.Ticket.update);

router.put('/ticket/:id/status', Controller.Ticket.changeStatus)

module.exports = router;
