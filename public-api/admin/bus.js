
/*
	@author: Ankit Singh<ankit@wizely.in>
 */

const Controller = require('../../server/controllers')
const router 	 = require('express').Router()


router.route('/bus/:id/reset')
	.post(Controller.Bus.resetAvailability);

module.exports = router;
