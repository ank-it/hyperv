/* 
 * File to init the server and connect to 
 * Database.
 * @author: Ankit Singh<ank.it@live.com> 
 */


const mongoose = require('mongoose');
const util = require('util');
const config = require('./config/env');
const app = require('./config/express');


console.log("Db connection: ", config.db);

var options = {}
// connect to mongo db
mongoose.connect(config.db);
mongoose.connection.on('error', function(err) {
  throw new Error('unable to connect to database:', config.db);
});


// print mongoose logs in dev env
if (config.MONGOOSE_DEBUG) {
  mongoose.set('debug', (collectionName, method, query, doc) => {
    // debug(`${collectionName}.${method}`, util.inspect(query, false, 20), doc);
  });
}



var server = app.listen(config.port, function(err) {
    console.log('Server started on',  config.port);
 });

exports.listen = function () {
  this.server.listen.apply(this.server, arguments);
};

exports.close = function (callback) {
  this.server.close(callback);
};