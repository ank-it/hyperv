
/*
 * Basically scenario was tested out with new bus and 
 * tickets assigned in it. Also on updating the seats availability.
 * @author: Ankit Singh<ank.it@live.com>
 */

process.env.NODE_ENV = 'test'

const assert = require('assert');
var Bus = require('../server/models/bus.js')
var server = require('../server.js')
const chai = require('chai');
const chaiHttp = require('chai-http');
const should = chai.should();
const BASE_ENDPOINT = 'http://localhost:3000/api/v1'

chai.use(chaiHttp);

var busId = ''
var ticketId = ''


describe('Bus', () => {

  before((done) => {
    Bus.remove({}, (err) => { 
       done();         
    });     
  });

  // Get all buses before insertiion
  describe('/GET Bus', () => {
    it('should return all the buses', (done) => {
      chai.request(BASE_ENDPOINT) 
          .get('/bus')
          .end((err, res) => {
            res.should.have.status(200)
            res.body.should.be.a('array');
            res.body.length.should.be.eql(0);
            done()
          })
    });
  });

  // Create a bus in the system
  describe('/POST Bus', () => {
    it('should create a bus', (done) => {
      var bus = {
        "name": "Kalka Bus Service",
        "type": "Volvo",
        "regNumber": "KA01556655",
        "capacity": 1,
        "availableSeats": 1,
        "from": "Banglore",
        "to": "Mysore",
        "tripsCompleted": 10 
      }
      chai.request(BASE_ENDPOINT) 
          .post('/bus')
          .send(bus)
          .end((err, res) => {
            busId = res.body._id
            res.should.have.status(200);
            res.body.should.be.a('object');
            res.body.should.have.property('name');
            res.body.should.have.property('capacity');
            res.body.should.have.property('availableSeats');
            res.body.should.have.property('createdAt');
            done()
          })
    });
  });

   // Create a ticket in the bus 
  describe('/POST ticket', () => {
    it('should create a ticket', (done) => {
      var ticket = {
          "user": {
            "name": "Ankit",
            "emailId": "ankit@gmail.com",
            "phoneNumber": "0000000000"
            },
          "busId": busId,
          "status": true
      }
      chai.request(BASE_ENDPOINT) 
          .post('/ticket')
          .send(ticket)
          .end((err, res) => {
            ticketId = res.body._id
            res.should.have.status(200);
            res.body.should.be.a('object');
            res.body.user.should.have.property('name');
            res.body.should.have.property('status');
            res.body.should.have.property('busId').eql(busId);
            res.body.should.have.property('createdAt');
            done()
          })
    });
  });


   // Create a ticket in the bus 
  describe('/POST ticket', () => {
    it('should throw error when another ticket os created', (done) => {
      var ticket = {
          "user": {
            "name": "Ankit",
            "emailId": "ankit@gmail.com",
            "phoneNumber": "0000000000"
            },
          "busId": busId,
          "status": true
      }
      chai.request(BASE_ENDPOINT) 
          .post('/ticket')
          .send(ticket)
          .end((err, res) => {
            res.should.have.status(500);
            res.body.should.be.a('object');
            res.body.should.have.property('error');
            res.body.error.should.have.property('message').eql('Seats are full. Try next time.');
            done()
          })
    });
  });

  describe('/GET/:id ticket', () => {
    it('should return the ticket details', (done) => {

      done()

    })
  })

  // Update a ticket status from true to false 
  describe('/PUT/:id ticket', () => {
    it('should update the ticket status to false', (done) => {
      
      var status = {
        status: false
      }
      chai.request(BASE_ENDPOINT) 
          .put('/ticket/' + ticketId)
          .send(status)
          .end((err, res) => {
            res.should.have.status(200);
            res.body.should.be.a('object');
            res.body.should.have.property('status').eql(false);
            done()
          })
    });
  });


  // Post updating the ticket status of previous one, I should be 
  // able to book the seat now. 
  describe('/POST ticket', () => {
    it('should give the ticket this time as seat is vaccant', (done) => {
      var ticket = {
          "user": {
            "name": "Ankit",
            "emailId": "ankit@gmail.com",
            "phoneNumber": "0000000000"
            },
          "busId": busId,
          "status": true
      }
      chai.request(BASE_ENDPOINT) 
          .post('/ticket')
          .send(ticket)
          .end((err, res) => {
            res.should.have.status(500);
            res.body.should.be.a('object');
            res.body.should.have.property('error');
            res.body.error.should.have.property('message').eql('Seats are full. Try next time.');
            done()
          })
    });
  });


});