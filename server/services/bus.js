
/*
 * @author: Ankit Singh <ank.it@live.com>
 */

var Bus = require('../models/bus.js')

module.exports = {

  create: (json) => {
    var bus = new Bus(json)
    return bus.save()
  },

  find: (filter) => {
    return Bus.find(filter)
  },

  findById: (id) => {
    return Bus.findOne({_id: id});
  },

  findOneByFilter: (filter) => {
    return Bus.findOne(filter);
  },

	update: (id, update) => {
    return Bus.findOneAndUpdate(id, {$set: update}, {new: true});
  },

  checkAvailability: (busId) => {
    return Bus.findById(busId)
      .then((bus) => {
        if (bus.availableSeats > 0) {
          return Promise.resolve(true)
        }
        else {
          return Promise.resolve(false)
        }
      })
      .catch((err)=> {
        return Promise.reject(err)
      })
  }, 

  increaseSeats: (busId) => {
    Bus.findById(busId)
      .then((bus) => {
        bus.availableSeats += 1
        return bus.save()
      })
      .catch((err) => {
        return err
      })
  },

  decreaseSeats: (busId) => {
    Bus.findById(busId)
      .then((bus) => {
        bus.availableSeats -= 1
        return bus.save()
      })
      .catch((err) => {
        return err
      })
  },

  resetSeats: (busId) => {
    return Bus.findById(busId)
      .then((bus) => {
        bus.availableSeats = bus.capacity
        return bus.save()
      })
      .catch((err) => {
        return err
      })
  }

}


















