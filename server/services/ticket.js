
/*
 * @author: Ankit Singh <ank.it@live.com>
 */

var Ticket = require('../models/ticket.js')
const BusService = require('./bus.js')

var self = module.exports = {

  create: (json) => {
    return BusService.checkAvailability(json.busId)
      .then((status) => {
        if (status) {
          var ticket = new Ticket(json)
          return ticket.save() 
        }
        else {
          throw new Error('Seats are full. Try next time.')
        }        
      })
  },

  find: (filter) => {
    return Ticket.find(filter)
  },

  findById: (id) => {
    return Ticket.findOne({_id: id});
  },

  findOneByFilter: (filter) => {
    return Ticket.findOne(filter);
  },

	update: (id, update) => {
    return Ticket.findOneAndUpdate(id, {$set: update}, {new: true});
  },

  changeStatus: (ticketId, status) => {
    return self.findById(ticketId)
      .then((ticket) => {
        if (ticket.status === status) {
          console.log('Inside tis is ')
          return Promise.resolve(ticket)
        }
        else if(status === false) {
          BusService.increaseSeats(ticket.busId)
          return self.update(ticketId, { status: false })
        }
        else {
          BusService.decreaseSeats(ticket.busId)
          return self.update(ticketId, { status: true })
        }
      })
  }
}


















