/* 
 * 24th Jun, 2018
 * @author: Ankit Singh<ankit@wizely.in>
 */

var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var Bus = require('../services/bus.js')

var TicketSchema = new Schema({
	user: {
		name: String,
		emailId: String,
		phoneNumber: String
	}, // Should be a different object but for simplicity
  busId: {
		type: Schema.Types.ObjectId,
		ref: 'Bus'
	},
  status: Boolean 
},{
    timestamps: true
});


TicketSchema.pre('save', (next) => {
	next()
	// Bus.checkAvailability(doc.busId)
	// 	.then((status) => {
	// 		if (status) {
	// 			next()
	// 		}
	// 		else {
	// 			var err = new Error('Seats are full')
	// 			next(err)
	// 		}
	// 	})
	// 	.catch((err) => {
	// 		next(err)
	// 	})
});

TicketSchema.post('save', (doc) => {
	Bus.decreaseSeats(doc.busId)
});

module.exports = mongoose.model('ticket', TicketSchema);
