/* 
 * 24th Jun, 2018
 * @author: Ankit Singh<ankit@wizely.in>
 */

var mongoose = require('mongoose');
var Schema = mongoose.Schema;


var BusSchema = new Schema({
	name: String,
	type: String,
	regNumber: String,
	capacity: {
		type: Number,
		validate: {
      validator: (value) => {
          return value > 0;
      },
      message: 'Amazing! You can run empty bus, I can\'t'
    }
	},
	availableSeats: Number,
	from: String,
	to: String,
  tripsCompleted: Number 
}, {
    timestamps: true
});


module.exports = mongoose.model('bus', BusSchema);
