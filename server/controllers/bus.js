/* 
 * 24th Jun, 2018
 * @author: Ankit Singh<ank.it@live.com>
 */

const BusService = require('../services/bus.js')


module.exports = {

	get: (req, res, next) => {
		// TODO: skip and limit to be added
		BusService.find()
							.then((buses) => {
								res.send(buses)
							})
							.catch((err) => {
								next(err)
							})
	},

	find: (req, res, next) => {
		BusService.findByFilter({ userId: req.params.id })
			.then((result)=> {
				res.send(result)
			}) 
			.catch((err) => {
				next(err)
			});
	},

	create: (req, res, next) => {
		var body = req.body;

		BusService.create(body)
			.then((goal) => {
				res.send(goal);
			})
			.catch((err) => {
				next(err);
			})
	},

	resetAvailability: (req, res, next) => {
		BusService.resetSeats(req.params.id)
			.then((bus) => {
				res.send(bus)
			})
			.catch((err) => {
				next(err)
			})
	}

}
