/* 
 * 24th Jun, 2018
 * @author: Ankit Singh<ank.it@live.com>
 */

const TicketService = require('../services/ticket.js')

module.exports = {

	get: (req, res, next) => {
		// TODO: skip and limit to be added
		var filter = req.query

		TicketService.find(filter)
			.then((buses) => {
				res.send(buses)
			})
			.catch((err) => {
				next(err)
			})
	},

	find: (req, res, next) => {
		// var filters = 
		TicketService.findOneByFilter({ _id: req.params.id })
			.then((result)=> {
				res.send(result)
			}) 
			.catch((err) => {
				res.send("Internal server error hui h :P")
			});
	},

	create: (req, res, next) => {
		var body = req.body;

		TicketService.create(body)
			.then((goal) => {
				res.send(goal);
			})
			.catch((err) => {
				next(err);
			})
	},

	/*
	 * Update the ticket status with the 
	 * business logic of making the seats avail
	 * -able if it is vacated else decrese avail-
	 * ability
	 * 
	 */
	update: (req, res, next) => {
		var id = req.params.id

		TicketService.update(id, req.body)
			.then((ticket) => {
					res.send(ticket)	
			})
			.catch((err) => {
					next(err)
			})
	},

	changeStatus: (req, res, next) => {
		var id = req.params.id

		TicketService.changeStatus(id, req.body.status)
			.then((ticket) => {
					res.send(ticket)	
			})
			.catch((err) => {
					next(err)
			})
	}
}
